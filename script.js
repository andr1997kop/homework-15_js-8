/* 
1. Це дерево об'єктів документа, що дозволяє програмісту 
звернутись до будь-якого тегу на сторінці та виконати з ним
певні дії.
2. innerHTML дозволяє отримати чи змінити також HTML-елемент, 
а innerText - лише текстовий зміст елемента
3. Існує два способи - querySelector, який є більш універсальним та зручним
та getElementby(id/class тощо), який є швидшим. Як на мене querySelector - 
зручніший варіант.
 */

const paragraphs = document.querySelectorAll(`p`);
const paragraphs2 = document.getElementsByTagName(`p`)
paragraphs.forEach((elem) => elem.style.background = "#ff0000");
const optionsList = document.querySelector(`.options-list`);

console.log(optionsList);
console.log(optionsList.parentNode)
console.log(optionsList.childNodes);
const testPar = document.querySelector(`#testParagraph`);
testPar.innerText = "This is a paragraph";

const mainHeader = document.querySelector(`.main-header`);
const mainHeaderChildren = mainHeader.children;
console.log(mainHeaderChildren);

function addClass(param){
    for (let i = 0; i < mainHeaderChildren.length; i++){
        mainHeaderChildren[i].classList.add(`nav-item`)
    }
}

addClass();
console.log(mainHeaderChildren);

const sectionTitle = document.querySelectorAll(`.section-title`);

function removeClass(param){
    for (let i = 0; i < sectionTitle.length; i++){
        sectionTitle[i].classList.remove(`section-title`)
    }
}
removeClass();
console.log(sectionTitle);